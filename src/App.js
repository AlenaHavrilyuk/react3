import Chat from '../src/components/chat/Chat';
import { BrowserRouter, Route } from "react-router-dom";
import EditModal from "./components/editModal/EditModal";
import LoginForm from "./components/users/loginForm/LoginForm";
import UserList from "./components/users/userList/UserList";
import EditUserForm from "./components/users/userEditForm/EditUserForm";
import Exception from './exceptions/Exception';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Exception/>
        <Route path='/chat' component={Chat}/>
        <Route path='/edit' component={EditModal}/>
        <Route path='/' component={LoginForm}/>
        <Route path='/userList' component={UserList}/>
        <Route path='/editUser' component={EditUserForm}/>
      </div>
    </BrowserRouter>
    
  );
}

export default App;
