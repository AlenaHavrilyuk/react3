import { setPreloader } from '../../components/chat/actions';
import {DELETE_USER} from '../../components/users/userList/actionType';
import { takeEvery, put, call } from '@redux-saga/core/effects';
import { loadUsers } from '../../components/users/actions';
import { showException } from '../../exceptions/actions';

async function deleteData(url = '') {
    const response = await fetch(url, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    return await response.json();
  }

export function* workerSaga(action) {
    yield put(setPreloader());
    try{
      yield call(deleteData, 'http://localhost:3004/users/' + action.userId);
    }
    catch (error) {
      yield put(showException('Cant delete user'));
    }
    yield put(loadUsers());
}

export function* deleteUserSaga() {
    yield takeEvery(DELETE_USER, workerSaga);
}