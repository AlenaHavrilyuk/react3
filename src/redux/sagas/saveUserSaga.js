import { SAVE_USER } from '../../components/users/actionType';
import { takeEvery, put, call, select } from '@redux-saga/core/effects';
import { closeEditUserForm, loadUsers } from '../../components/users/actions';
import { editUserName, editPassword, editUserById }  from '../../components/users/userEditForm/actions'
import idCreator from '../../components/utils/idCreator';
import { showException } from '../../exceptions/actions';

async function putData(url = '', data = {}) {
    const response = await fetch(url, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    return await response.json();
}

async function postData(url = '', data = {}) {
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    return await response.json();
}

export function* workerSaga() {

    //get data from edit user form
    const getUserId = state => state.editUser.userId;
    let id = yield select(getUserId);
    const getUserName = state => state.editUser.userName;
    const userName = yield select(getUserName);
    const getPassword = state => state.editUser.password;
    const password = yield select(getPassword);

    let user;

    if (!id) {
        //create new user
        id = idCreator();
        user = {
            id: id,
            login: userName,
            password: password,
            role: user,
            avatar: ""
        }
        try{
            yield call(postData, 'http://localhost:3004/users', user);
        }
        catch (error) {
            yield put(showException('Cant sand massage'));
        }
    } else {
        //update user
        const getUsers = state => state.users.users;
        const users = yield select(getUsers);
        users.forEach((element) => {
            if (element.id === id) {
                user = element;
            }
        })
        user.login = userName;
        user.password = password;

        try{
            yield call(putData, 'http://localhost:3004/users/' + id, user);
        }
        catch (error) {
            yield put(showException('Cant sand massage'));
        }
    }
    yield put(editUserById(""));
    yield put(editUserName(""));
    yield put(editPassword(""))

    yield put(closeEditUserForm());
}

export function* saveUserSaga() {
    yield takeEvery(SAVE_USER, workerSaga);
}