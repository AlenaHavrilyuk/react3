import { setLoadedData, setPreloader } from '../../components/chat/actions';
import {LOAD_DATA} from '../../components/chat/actionType';
import { takeEvery, put, call, select } from '@redux-saga/core/effects';
import { showException } from '../../exceptions/actions';

async function getData(){
    const request = await fetch('http://localhost:3004/messages');
    const data = await request.json();
    return data;
}

export function* workerSaga() {
    const getPreloader = state => state.chat.preloader
    const preloader = yield select(getPreloader);
    if(!preloader){
        yield put(setPreloader());
    }
    try{
        const data = yield call(getData);
        yield put(setLoadedData(data)); 
    }
    catch (error) {
        yield put(showException('Cant load data'));
    }
    yield put(setPreloader());
}

export function* loadDataSaga() {
    yield takeEvery(LOAD_DATA, workerSaga);
}