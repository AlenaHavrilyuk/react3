import { setPreloader, loadData } from '../../components/chat/actions';
import {DELETE_MESSAGE} from '../../components/chat/actionType';
import { takeEvery, put, call } from '@redux-saga/core/effects';
import { showException } from '../../exceptions/actions';

async function deleteData(url = '') {
    const response = await fetch(url, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    return await response.json();
  }

export function* workerSaga(action) {
    yield put(setPreloader());
    try{
      yield call(deleteData, 'http://localhost:3004/messages/' + action.messageId);
    }
    catch (error) {
      yield put(showException('Cant delete massage'));
    }
    
    yield put(loadData());
}

export function* deleteMessageSaga() {
    yield takeEvery(DELETE_MESSAGE, workerSaga);
}