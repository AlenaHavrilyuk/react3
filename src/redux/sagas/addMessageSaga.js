import { setPreloader, loadData } from '../../components/chat/actions';
import {ADD_MESSAGE} from '../../components/chat/actionType';
import { takeEvery, put, call } from '@redux-saga/core/effects';
import { showException } from '../../exceptions/actions';

async function postData(url = '', data = {}) {
  const response = await fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
  if(Math.random() > 0.5) throw new Error();
  return await response.json();
}

export function* workerSaga(action) {
  yield put(setPreloader());

  try {
    yield call(postData, 'http://localhost:3004/messages', action.message);
  }
  catch (error) {
    yield put(showException('Cant sand massage'));
  }

  yield put(loadData());
}

export function* addMessageSaga() {
    yield takeEvery(ADD_MESSAGE, workerSaga);
}