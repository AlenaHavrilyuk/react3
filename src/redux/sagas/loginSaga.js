import { tryLoginUser } from '../../components/users/actions';
import { showException } from '../../exceptions/actions';
import { LOGIN_USER } from '../../components/users/actionType';
import { takeEvery, put } from '@redux-saga/core/effects';

export function* tryLoginWorkerSaga(action) {
    try{
        yield put(tryLoginUser(action.userData.userName, action.userData.password));
    }
    catch(error){
        yield put(showException(error));
    }
}

export function* loginSaga() {
    yield takeEvery(LOGIN_USER, tryLoginWorkerSaga);
}