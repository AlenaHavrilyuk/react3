import { loadDataSaga } from "./loadDataSaga";
import { addMessageSaga } from "./addMessageSaga";
import { deleteMessageSaga  } from "./deleteMessageSaga";
import { spawn } from '@redux-saga/core/effects';
import { editMessageSaga } from "./editMessageSaga";
import { loadUsersSaga } from "./loadUsersSaga";
import { loginSaga} from "./loginSaga";
import { deleteUserSaga } from "./deleteUserSaga";
import { saveUserSaga } from "./saveUserSaga";
import { closeLoginFormSaga } from "./closeLoginFormSaga";

export default function* rootSaga(){
    yield spawn(loadDataSaga);
    yield spawn(addMessageSaga);
    yield spawn(deleteMessageSaga);
    yield spawn(editMessageSaga);
    yield spawn(loadUsersSaga);
    yield spawn(loginSaga);
    yield spawn(deleteUserSaga);
    yield spawn(saveUserSaga);
    yield spawn(closeLoginFormSaga);
}