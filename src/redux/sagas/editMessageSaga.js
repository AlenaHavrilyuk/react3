import { setPreloader, closeEditModal } from '../../components/chat/actions';
import {SAVE_EDITED_MESSAGE} from '../../components/chat/actionType';
import { takeEvery, put, call, select } from '@redux-saga/core/effects';
import { showException } from '../../exceptions/actions';

async function postData(url = '', data = {}) {
    const response = await fetch(url, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });
    return await response.json();
  }

export function* workerSaga(action) {
    yield put(setPreloader());
    const getMessages = state => state.chat.messages;
    const messages = yield select(getMessages);
    const getCurrentUserId = state => state.users.currentUserId;
    const currentUserId = yield select(getCurrentUserId);
    let lastOwnMessage;
    for (let index = messages.length - 1; index >= 0; index--) {
        if (messages[index].userId === currentUserId) {
            lastOwnMessage = messages[index];
            break;
        }
    }
    lastOwnMessage.text = action.text;
    try{
      yield call(postData, 'http://localhost:3004/messages/' + lastOwnMessage.id, lastOwnMessage);
    }
    catch (error) {
      yield put(showException('Cant save massage'));
    }
    
    yield put(closeEditModal());
}

export function* editMessageSaga() {
    yield takeEvery(SAVE_EDITED_MESSAGE, workerSaga);
}