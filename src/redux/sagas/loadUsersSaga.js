import { setPreloader } from '../../components/chat/actions';
import { setLoadedUsers } from '../../components/users/actions';
import { LOAD_USERS } from '../../components/users/actionType';
import { takeEvery, put, call, select } from '@redux-saga/core/effects';
import { showException } from '../../exceptions/actions';

async function getData(){
    const request = await fetch('http://localhost:3004/users');
    const data = await request.json();
    return data;
}

export function* workerSaga() {
    const getPreloader = state => state.chat.preloader
    const preloader = yield select(getPreloader);
    if(!preloader){
        yield put(setPreloader());
    }
    try{
        const data = yield call(getData);
        yield put(setLoadedUsers(data)); 
    }
    catch (error) {
        yield put(showException('Cant sand massage'));
    }
    
    yield put(setPreloader());
}

export function* loadUsersSaga() {
    yield takeEvery(LOAD_USERS, workerSaga);
}