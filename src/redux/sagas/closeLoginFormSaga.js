import { setPreloader } from '../../components/chat/actions';
import { CLOSE_LOGIN_FORM } from '../../components/users/actionType';
import { takeEvery, put, select } from '@redux-saga/core/effects';

export function* closeFormWorkerSaga() {
    const getRole = state => state.users.currentUserRole;
    const role = yield select(getRole);
    if(role === "user"){
        yield put(setPreloader());
    }    
}

export function* closeLoginFormSaga() {
    yield takeEvery(CLOSE_LOGIN_FORM, closeFormWorkerSaga);
}