import { combineReducers } from "redux";
import chat from '../../components/chat/reducer'
import input from '../../components/messageInput/reducer';
import editedMessage from '../../components/editModal/reducer';
import loginForm from '../../components/users/loginForm/loginFormReducer';
import users from '../../components/users/usersReducer';
import editUser from '../../components/users/userEditForm/reducer';
import exception from '../../exceptions/reducer';

const reducer = combineReducers(
    {
        chat,
        input,
        editedMessage,
        loginForm,
        users,
        editUser,
        exception
    }
);

export default reducer;