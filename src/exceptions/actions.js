import { EXCEPTION, CLOSE_EXCEPTION } from "./actionType";

export const showException = (value) => ({type: EXCEPTION, exception: value});
export const closeException = (value) => ({type: CLOSE_EXCEPTION, exception: value});
