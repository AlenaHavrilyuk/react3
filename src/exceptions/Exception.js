import { useSelector, useDispatch } from "react-redux";
import './exception.css';
import { useHistory } from "react-router-dom";
import { closeException} from './actions';

export default function Exception (){
    let isError = useSelector(state => state.exception.isError);
    let value = useSelector(state => state.exception.value);
    const history = useHistory();
    const dispatch = useDispatch();

    function routeChange () { 
        dispatch(closeException());
        let path = '/'; 
        history.push(path);
    }

    return (
        <form className={isError ? "exception show-exception" : "exception"}>
            <div>
                <div>ERROR:</div>  
                <div>{value}</div>
            </div>
            <button className="orange-button" onClick={routeChange}> Reload </button>
        </form>
    )
}