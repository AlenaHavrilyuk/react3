import { EXCEPTION, CLOSE_EXCEPTION } from "./actionType";

const initialState = {
    value: "",
    isError: false
}

export default function (state=initialState, action){
    switch(action.type) {
        case EXCEPTION: {
            return Object.assign({}, state, {
                value : action.exception,
                isError: true
            })
        }
        case CLOSE_EXCEPTION: {
            return Object.assign({}, state, {
                value : "",
                isError: false
            })
        }
        default:
            return state;
    }
}