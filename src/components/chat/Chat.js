import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Header from '../header/Header';
import MessageList from '../messages/MessageList';
import Preloader from '../preloader/Preloader';
import './chat.css'
import MessageInput from '../messageInput/MessageInput';
import { loadData } from './actions';
import { Redirect } from "react-router-dom";


export default  function Chat (props) {
    const chat = useSelector((state) => state.chat);
    const isLogin = useSelector((state) => state.users.isLogin);
    const dispatch = useDispatch();

    useEffect(() =>{dispatch(loadData())}, []);

    if(!isLogin) {
        return(<Redirect to='/'/>);
    }

    if(chat.preloader){
        return (
            <div className="chat">
                <Preloader />
            </div>
        )
    }
        else{  
            let data = chat.messages;
            if(data.length === 0){
                dispatch(loadData());
            }
            let messCount = data.length;
            let userSet = new Set();
            data.forEach(function(item) {
                userSet.add(item.userId);
            });
            let userCount = userSet.size;
            let lastMessDate = data[data.length - 1].createdAt;

            if(chat.editModal) {
                return(<Redirect to='/edit'/>);
            }
            return (
                <div className="chat">
                    <Header messagesCount={messCount} usersCount={userCount} lastMessageDate={lastMessDate}/>
                    <MessageList messages={data}/> 
                    <MessageInput />
                </div>
            )
        }   
    }
