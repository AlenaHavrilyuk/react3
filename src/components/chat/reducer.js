import { SET_LOADED_DATA, SET_PRELOADER, CALL_EDIT_MODAL, CLOSE_EDIT_MODAL } from './actionType';

const initialState = {
    messages: [],
    editModal: false,
    preloader: true
}

export default function (state=initialState, action){
    switch(action.type) {
        case SET_LOADED_DATA: {
            return Object.assign({}, state, {
                ...state,
                messages: action.data,
            })
        }

        case SET_PRELOADER: {
            return Object.assign({}, state, {
                ...state,
                preloader: !state.preloader
            })
        }
        
        case CALL_EDIT_MODAL: {
            return Object.assign({}, state, {
                ...state,
                editModal: true
            })
        }
        
        case CLOSE_EDIT_MODAL : {
            return Object.assign({}, state, {
                ...state,
                editModal: false 
            })
        }

        default:
            return state;
    }
}