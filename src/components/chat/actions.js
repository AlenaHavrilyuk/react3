import { SET_LOADED_DATA, SET_PRELOADER, LOAD_DATA, ADD_MESSAGE, DELETE_MESSAGE, CALL_EDIT_MODAL, SAVE_EDITED_MESSAGE, CLOSE_EDIT_MODAL } from './actionType';

export const setLoadedData = (data) => ({type: SET_LOADED_DATA, data: data});
export const addMessage = (message) => ({type: ADD_MESSAGE, message: message});
export const deleteMessage = (messageId) => ({type: DELETE_MESSAGE, messageId: messageId});
export const callEditModal = () => ({type: CALL_EDIT_MODAL});
export const saveEditedMessage = (text) => ({type: SAVE_EDITED_MESSAGE, text: text});
export const closeEditModal = () => ({type: CLOSE_EDIT_MODAL});
export const loadData = () => ({type: LOAD_DATA});
export const setPreloader = () => ({type: SET_PRELOADER});