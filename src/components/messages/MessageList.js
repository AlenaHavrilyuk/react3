import { isAfter, isSameDay, startOfYear, subDays, format, parseISO } from 'date-fns';
import Message from './Message';
import './messageList.css'
import OwnMessage from './OwnMessage';
import Divider from './Divider';
import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';

export default function MessageList(props) {

  const messagesEndRef = React.createRef()
  const currentUserId = useSelector((state) => state.users.currentUserId);

  function scrollToBottom() {
    messagesEndRef.current.scrollIntoView({ behavior: 'smooth' })
  }

  useEffect(() => { scrollToBottom() });

  const msgList = [];
  let prevMessageDate = null;
  let lastOwnMessageId;
  for (let index = props.messages.length - 1; index >= 0; index--) {
    if (props.messages[index].userId === currentUserId) {
      lastOwnMessageId = props.messages[index].id;
      break;
    }
  }

  props.messages.forEach((msg) => {
    let date = formatDate(msg.createdAt);

    if (date !== prevMessageDate) {
      msgList.push(
        <Divider key={"_" + date} date={date} />
      );
    };
    prevMessageDate = date;

    if (msg.id === lastOwnMessageId) {
      msgList.push(
        <OwnMessage
          key={msg.id}
          message={msg}
          canBeEdited={true}
        />
      );
    } else if (msg.userId === currentUserId) {
      msgList.push(
        <OwnMessage
          key={msg.id}
          message={msg}
          canBeEdited={false}
        />
      );
    } else {
      msgList.push(
        <Message
          key={msg.id}
          message={msg}
        />
      );
    }
  })

  return (
    <div className="message-list">
      {msgList}
      <div className="messages-end" ref={messagesEndRef} />
    </div>
  )
}

function formatDate(dateToBeFormatted) {
    const today = new Date();
    const yesterday = subDays(today, 1);
    const startOfThisYear = startOfYear(today);
    const date = parseISO(dateToBeFormatted);

    let result = "";
    if (isSameDay(date, today)) {
      result = "today";
    } else if (isSameDay(date, yesterday)) {
      result = "yesterday";
    } else if (isAfter(date, startOfThisYear)) {
      result = format(date, 'eeee, MMMM do');
    } else {
      result = format(date, 'eeee, MMMM do yyyy');
    }

    return result;
  }