import React from 'react';
import './message.css';
import { format } from 'date-fns';

export default function Message (props){
    let data = props.message;
    let edited = "";
    let datetime;
    if(data.editedAt === "") {
        datetime = new Date(data.createdAt);
    } else{
        datetime = new Date(data.editedAt);
        edited = "edited";
    }
    let time = format(datetime, 'kk:mm');
    
    return(
    <div className="message">
        <div className="message-text">
            {data.text}
        </div>
        <div className="message-time">
            {time} {edited}
        </div>
        <div className="message-user-name">
            {data.user}
        </div>
        <div className="message-user-avatar">
            <img src={data.avatar ? data.avatar : "https://okeygeek.ru/wp-content/uploads/2020/03/no_avatar.png"}/>
        </div>
        <div className="message-background"></div>
    </div>
    )
}