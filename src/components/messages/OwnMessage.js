import React from 'react';
import { format } from 'date-fns';
import {deleteMessage, callEditModal} from '../chat/actions';
import { editText } from '../editModal/actions'
import { useDispatch } from 'react-redux';

export default function OwnMessage(props) {
    const dispatch = useDispatch();
    function editMessage() {
        dispatch(editText(props.message.text));
        dispatch(callEditModal());
    }

    function deleteThis() {
        dispatch(deleteMessage(props.message.id));
    }
    let data = props.message;
    let edited = "";
    let datetime = new Date(data.createdAt)
    if (data.editedAt !== "") {
        edited = "edited";
    }
    let time = format(datetime, 'kk:mm');
    let canBeEdited = props.canBeEdited;
    return (
        <div className={canBeEdited ? "own-message can-be-edited" : "own-message"}>
            <div className="message-text">
                {data.text}
            </div>
            <div className="message-time">
                {time} {edited}
            </div>
            <button className="message-edit" onClick={editMessage}>
                edit
            </button>
            <button className="message-delete" onClick={deleteThis}>
                delete
            </button>
            <div className="message-background"></div>
        </div>
    )
}
