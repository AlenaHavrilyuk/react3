import React from 'react';

export default function Divider (props){
 return(
        <div className="messages-divider">
            {props.date}
        </div>
    )
}