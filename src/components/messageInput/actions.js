import { SET_INPUT_VALUE } from "./actionType";

export const setInputValue = (value) => ({type: SET_INPUT_VALUE, value: value});
