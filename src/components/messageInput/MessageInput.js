import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './messageInput.css';
import idCreator from '../utils/idCreator';
import { addMessage } from '../chat/actions';
import { setInputValue } from '../messageInput/actions'

export default function MessageInput (props){
    const input = useSelector((state) => state.input);
    const currentUserId = useSelector((state) => state.users.currentUserId);
    const currentUserName = useSelector((state) => state.users.currentUserName);
    const dispatch = useDispatch();

    const onSubmit = (event) => { 
        const newMassage = {
            id: idCreator(),
            userId: currentUserId,
            avatar: "",
            user: currentUserName,
            text: input.value,
            createdAt: new Date().toISOString(),
            editedAt: ""
        }
        dispatch(addMessage(newMassage));
        dispatch(setInputValue(""));
        event.preventDefault();
    }

    const handleChange = (event) => {  
        dispatch(setInputValue(event.target.value));
    }

    
    return(
    <form className="message-input" onSubmit={onSubmit}>
        <input type="text" value={input.value} onChange={handleChange}></input>
        <button type="submit">Send</button>
    </form>
    )
}