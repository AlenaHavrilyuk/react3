import { SET_INPUT_VALUE } from "./actionType";

const initialState = {
    value: ""
}

export default function (state=initialState, action){
    switch(action.type) {
        case SET_INPUT_VALUE: {
            return Object.assign({}, state, {
                value : action.value
            })
        }
        default:
            return state;
    }
}
