import React from 'react';
import preloader from '../images/preloader.gif'
import './preloader.css'

export default function Preloader(){
    return (
        <div className="preloader">
            <img className="spinner" src={preloader} />
        </div>
    );
}
