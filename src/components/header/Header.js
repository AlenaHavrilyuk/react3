import React from 'react';
import './header.css';
import { format, parseISO } from 'date-fns';

export default function Header(props){
    let date = format(parseISO(props.lastMessageDate), 'dd.MM.yyyy kk:mm');
    return(
    <div className="header">
        <p className="header-title">
        My chat
        </p>
        <p className="header-users-count">
        {props.usersCount} participants
        </p>
        <p className="header-messages-count">
        {props.messagesCount} messages
        </p>
        <p className="header-last-message-date">
        last message at: {date}
        </p>
    </div>
    )
}