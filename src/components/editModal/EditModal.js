import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './editModal.css';
import { saveEditedMessage, closeEditModal } from '../chat/actions';
import { editText } from '../editModal/actions';
import { Redirect } from "react-router-dom";


export default function EditModal(props) {
    const editModal = useSelector((state) => state.chat.editModal);
    const editedMessage = useSelector((state) => state.editedMessage);
    const dispatch = useDispatch();

    function onSubmit(event) {
        dispatch(saveEditedMessage(editedMessage.text));
        event.preventDefault();
    }

    function onClose(event) {
        dispatch(closeEditModal());
        event.preventDefault();
    }

    function handleChange(event) {
        dispatch(editText(event.target.value));
    }
    let value = editedMessage ? editedMessage.text : "";
    if (!editModal) {
        return (<Redirect to='/chat' />);
    }
    return (
        <form className="edit-message-modal modal-shown" >
            <textarea className="edit-message-input" value={value} onChange={handleChange} />
            <button className="edit-message-button orange-button" onClick={onSubmit} >save</button>
            <button className="edit-message-close colorless-button" onClick={onClose}>close</button>
        </form>
    )
}