import { EDIT_TEXT } from "./actionType";

export const editText = (text) => ({type: EDIT_TEXT, text: text});