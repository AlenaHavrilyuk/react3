import { useDispatch, useSelector } from 'react-redux';
import './userList.css';
import { deleteUser } from './actions';
import { openEditUserForm, loadUsers } from '../actions';
import { setPreloader} from '../../chat/actions';
import Preloader from '../../preloader/Preloader';
import { editPassword, editUserName, editUserById } from '../userEditForm/actions';
import { Redirect, useHistory } from "react-router-dom";
import React, { useEffect } from 'react';

export default function UserList(){
    const users = useSelector(state => state.users.users);
    const preloader = useSelector((state) => state.chat.preloader);
    const editUserForm = useSelector(state => state.users.editUserForm);
    const userRole = useSelector(state => state.users.currentUserRole);
    const dispatch = useDispatch();

    const history = useHistory();

    useEffect(() =>{dispatch(loadUsers())}, []);

    function routeChange () { 
        dispatch(setPreloader());
        let path = '/chat'; 
        history.push(path);
    }
    
    function onClickEditButton(event){
        let editedUserName;
        let editedPassword;
        users.forEach(element => {
            if(element.id === event.target.accessKey){
                editedUserName = element.login;
                editedPassword = element.password;
            }
        });
        dispatch(editUserName(editedUserName));
        dispatch(editPassword(editedPassword));
        dispatch(editUserById(event.target.accessKey));
        dispatch(openEditUserForm());
    }

    function onClickDeleteButton(event){
        dispatch(deleteUser(event.target.accessKey));
    }

    function onClickAddUserButton(){
        dispatch(openEditUserForm());
    }

    const list = users.map((item) => 
    <li key={item.id}> 
        <span className="user-name">{item.login}</span>
        <span className="password">{item.password}</span>
        <button className="orange-button" accessKey={item.id} onClick={onClickEditButton}>Edit</button>
        <button className="colorless-button" accessKey={item.id} onClick={onClickDeleteButton}>Delete</button>
    </li>);
    if(userRole !== "admin" ){
        return (<Redirect to='/'/>);
    }
    if(preloader){
        return (
            <div className="chat">
                <Preloader />
            </div>
        )
    }
    if(editUserForm){
        return (<Redirect to='/editUser'/>);
    }

    return (
        <div className="userList">
            <div className="user-list-header">
                <span className="user-form-username-label"> Username </span> 
                <span className="user-form-password-label"> password </span> 
                <button className="orange-button add-user-button" onClick={onClickAddUserButton} > Add user </button>
                <button className="orange-button go-to-chat-button" onClick={routeChange}> Go to chat </button>
            </div>
           {list} 
        </div>
    )
}