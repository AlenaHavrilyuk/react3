import { DELETE_USER } from "./actionType";

export const deleteUser = (id) => ({type: DELETE_USER, userId: id});
