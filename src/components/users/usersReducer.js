import { SET_LOADED_USERS, TRY_LOGIN_USER, CLOSE_LOGIN_FORM, OPEN_EDIT_USER_FORM, CLOSE_EDIT_USER_FORM } from './actionType';

const initialState = {
    users: [],
    currentUserId: "",
    currentUserRole: "",
    currentUserName: "",
    isLogin: false,
    editUserForm: false
}

export default function (state=initialState, action){
    switch(action.type) {
        case SET_LOADED_USERS: {
            return Object.assign({}, state, {
                ...state,
                users: action.data
            })
        }
        case TRY_LOGIN_USER: {
            const userName = action.userData.userName;
            const password = action.userData.password;
            let currentUserId = "";
            let currentUserRole = "";
            let currentUserName = "";
            state.users.forEach(user =>{
                if(user.login === userName && user.password === password){
                    currentUserId = user.id;
                    currentUserRole = user.role;
                    currentUserName = user.login;
                }    
            })
            if(!currentUserId){
                throw "Such user doesn't exist";
            } else {
                return Object.assign({}, state, {
                ...state,
                currentUserId: currentUserId,
                currentUserRole: currentUserRole,
                currentUserName: currentUserName
            }) 
            }  
        }
        case CLOSE_LOGIN_FORM: {
            return Object.assign({}, state, {
                ...state,
                isLogin: true
            })
        }
        
        case OPEN_EDIT_USER_FORM: {
            return Object.assign({}, state, {
                ...state,
                editUserForm: true
            })
        }
        case CLOSE_EDIT_USER_FORM: {
            return Object.assign({}, state, {
                ...state,
                editUserForm: false
            })
        }

        default:
            return state;
    }
}