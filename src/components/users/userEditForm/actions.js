import { EDIT_USER_NAME, EDIT_PASSWORD, EDIT_USER_BY_ID } from "./actionType";

export const editUserName = (text) => ({type: EDIT_USER_NAME, text: text});
export const editPassword = (text) => ({type: EDIT_PASSWORD, text: text});
export const editUserById = (id) => ({type: EDIT_USER_BY_ID, id: id});
