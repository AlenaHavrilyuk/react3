import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import './editUser.css';
import { editUserName, editPassword, editUserById } from '../userEditForm/actions';
import { closeEditUserForm, saveUser } from '../actions';

import { Redirect } from "react-router-dom";


export default function EditUserForm(props) {
    const editUserForm = useSelector((state) => state.users.editUserForm);
    const editedUserName = useSelector((state) => state.editUser.userName);
    const editedPassword = useSelector((state) => state.editUser.password);
    const dispatch = useDispatch();

    function onSubmit(event) {
        dispatch(saveUser());
        event.preventDefault();
    }

    function onClose(event) {
        dispatch(editUserName(""));
        dispatch(editPassword(""));
        dispatch(editUserById(""));
        dispatch(closeEditUserForm());
        event.preventDefault();
    }

    function handleUserNameChange(event) {
        dispatch(editUserName(event.target.value));
    }
    function handlePasswordChange(event) {
        dispatch(editPassword(event.target.value));
    }

    if (!editUserForm) {
        return (<Redirect to='/userList' />);
    }
    return (
        <form className="edit-user-modal" >
            <label className="user-name-label">username</label>
            <textarea className="edit-username-input" value={editedUserName} onChange={handleUserNameChange} />
            <label className="password-label">password</label>
            <textarea className="edit-password-input" value={editedPassword} onChange={handlePasswordChange} />

            <button className="orange-button" onClick={onSubmit} >save</button>
            <button className="colorless-button" onClick={onClose}>close</button>
        </form>
    )
}