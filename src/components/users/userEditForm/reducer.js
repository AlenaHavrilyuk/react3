import { EDIT_USER_NAME, EDIT_PASSWORD, EDIT_USER_BY_ID } from "./actionType";

const initialState = {
    userName: "",
    password: "",
    userId: ""
}

export default function (state=initialState, action){
    switch(action.type) {
        case EDIT_USER_NAME: {
            return Object.assign({}, state, {
                ...state,
                userName: action.text
            })
        }
        case EDIT_PASSWORD: {
            return Object.assign({}, state, {
                ...state,
                password: action.text
            })
        }
        case EDIT_USER_BY_ID: {
            return Object.assign({}, state, {
                ...state,
                userId: action.id
            })
        }
        default:
            return state;
    }
    
}