import { LOAD_USERS, SET_LOADED_USERS, LOGIN_USER, TRY_LOGIN_USER, CLOSE_LOGIN_FORM, OPEN_EDIT_USER_FORM, CLOSE_EDIT_USER_FORM, SAVE_USER } from "./actionType";

export const loadUsers = () => ({type: LOAD_USERS});
export const loginUser = (userName, password) => (
    {
        type: LOGIN_USER, 
        userData: {userName: userName, password: password}}
    );
export const tryLoginUser = (userName, password) => (
    {
        type: TRY_LOGIN_USER, 
        userData: {userName: userName, password: password}}
    );
export const setLoadedUsers = (data) => ({type: SET_LOADED_USERS, data: data});
export const closeLoginForm = () => ({type: CLOSE_LOGIN_FORM});
export const openEditUserForm = () => ({type: OPEN_EDIT_USER_FORM});
export const closeEditUserForm = () => ({type: CLOSE_EDIT_USER_FORM});
export const saveUser = () => ({type: SAVE_USER});