import { SET_PASSWORD_VALUE, SET_USER_NAME_VALUE } from "./actionType";

const initialState = {
    userName: "",
    password: ""
}

export default function (state=initialState, action){
    switch(action.type) {
        case SET_USER_NAME_VALUE: {
            return Object.assign({}, state, {
                userName : action.value
            })
        }
        case SET_PASSWORD_VALUE: {
            return Object.assign({}, state, {
                password : action.value
            })
        }
        default:
            return state;
    }
}