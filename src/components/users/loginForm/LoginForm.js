import './loginForm.css'
import {setPasswordValue, setUserNameValue } from './actions'
import { loadUsers, closeLoginForm, loginUser} from '../actions';
import { useDispatch, useSelector } from 'react-redux';
import React, { useEffect } from 'react';
import { Redirect } from "react-router-dom";

export default function LoginForm () {
    const loginForm = useSelector((state) => state.loginForm);
    const dispatch = useDispatch();

    useEffect(() =>{dispatch(loadUsers())
    }, []);

    function handleChangeUserName(event){
        dispatch(setUserNameValue(event.target.value));
    }

    function handleChangePassword(event){
        dispatch(setPasswordValue(event.target.value));
    }

    function onSubmit(event){
        dispatch(loginUser(loginForm.userName, loginForm.password));
        dispatch(setUserNameValue(""));
        dispatch(setPasswordValue(""));
        event.preventDefault();
        dispatch(closeLoginForm());
    }

    const usersState = useSelector(state => state.users);
    if(usersState.isLogin){
        if(usersState.currentUserRole === 'user'){
            return(<Redirect to='/chat'/>);
        }
        if(usersState.currentUserRole === 'admin'){
            return(<Redirect to='/userList'/>);
        }
    }
    return (
        <form onSubmit={onSubmit} className="login">
            <fieldset >
                <legend className="legend">Login</legend>
                <div className="input">
                    <input onChange={handleChangeUserName} value={loginForm.userName} type="userName" placeholder="User name" required />
                    <span><i className="fa fa-envelope-o"></i></span>
                </div>
                <div className="input">
                    <input onChange={handleChangePassword} value={loginForm.password}type="password" placeholder="Password" required />
                    <span><i className="fa fa-lock"></i></span>
                </div>
                <button  type="submit" className="submit"><i className="fa fa-long-arrow-right"></i></button>
            </fieldset>
            <div className="feedback">
                login successful <br />
                redirecting...
            </div>

        </form>
    )
}