import { SET_USER_NAME_VALUE, SET_PASSWORD_VALUE } from './actionType';

export const setUserNameValue = (value) => ({type: SET_USER_NAME_VALUE, value: value});
export const setPasswordValue = (value) => ({type: SET_PASSWORD_VALUE, value: value});